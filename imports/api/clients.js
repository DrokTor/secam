import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Charts } from '../api/createdCharts.js';

import compile from 'monquery';

export const Clients = new Mongo.Collection('clients');
Clients.ClientsIDs = new Mongo.Collection('clients_ids');

if (Meteor.isServer) {

  // This code only runs on the server
Meteor.methods({


  //receive request
  'clients.addMachine'(client) {
    /*check(client,{
      name : String,
      os : String,
      ip : String,
    })*/


      client.state = 'Unknown';



    // Make sure the user is logged in before inserting a task
    /*if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }*/

    const DynTags = Meteor.call('tags.getDynamique') || [];
    //console.log(_.pluck(DynTags,'match'));
    const tagsArray = [];
    //console.log(DynTags);
    DynTags.map(( elem,index) => {
      const reg = new RegExp(  elem.match + ".*", 'i' );
      //console.log(reg.test(client.name));
      if(reg.test(client.name) || reg.test(client.os) || reg.test(client.ip) ){
        tagsArray.push(elem.name);
        //console.log(parseInt(elem.count)+1);
        Meteor.call('tags.match',elem.name,parseInt(elem.count)+1,elem.match)
      }
    });
    //console.log(tagsArray);

    const alias=(name)=>{
      const hostWords = name.split(' ');
      //console.log(hostWords);
      let alias = client.os;
      if(hostWords[1] == 'Windows' && hostWords[2] == 'Server'){
       alias = hostWords[1]+' '+hostWords[3];
     }else if (hostWords[1] == 'Windows'  ) {
       alias = hostWords[1]+' '+hostWords[2];
       //console.log(alias);
      }
      return alias;
    };
    //console.log(alias(client.os));
    const id = new Meteor.Collection.ObjectID();
    const updte= new Date();
    const aliasOS = alias(client.os);

    Clients.insert({
      _id: id,
      name: client.name,
      aliasOs: aliasOS,
      os: client.os,
      ip: client.ip,
      tags: tagsArray,
      state: client.state,
      lastUpdate: updte,
      scan:{
        min:moment(updte).format('mm').toString(),
        hour:moment(updte).format('HH').toString(),
        day:moment(updte).format('DD').toString(),
        month:moment(updte).format('MMM').toString(),
        year:moment(updte).format('YYYY').toString(),
      },
      process: client.process,
      software: client.software,
      });

    return id;

  },
  'clients.receive'(client,id) {
    /*check(client,{
      name : String,
      os : String,
      ip : String,
    })*/
    check(id,String)
    //console.log(client);
    switch(Math.floor((Math.random() * 10) + 1)%3) {
    case 0:
      client.state = 'Safe';
        break;
    case 1:
      client.state = 'Risky';
        break;
    default:
      client.state = 'Critical';
      }


    // Make sure the user is logged in before inserting a task
    /*if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }*/
    //console.log(id);
    const clientId= new Meteor.Collection.ObjectID(id);
    //console.log(clientId);
    const updte= new Date();

    Clients.update(clientId, {$set: {
      name: client.name,
      os: client.os,
      ip: client.ip,
      state: client.state,
      lastUpdate: updte,
      scan:{
        min:moment(updte).format('mm').toString(),
        hour:moment(updte).format('HH').toString(),
        day:moment(updte).format('DD').toString(),
        month:moment(updte).format('MMM').toString(),
        year:moment(updte).format('YYYY').toString(),
      },
      }},
      //{upsert : true},
    );
  },
  'clients.addTag'(clients,tag,match) {
    /*check(client,{
      name : String,
      os : String,
      ip : String,
    })*/
    //check(id,String)
    //console.log(client);



    // Make sure the user is logged in before inserting a task
    /*if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }*/

    //console.log(match);
    //console.log(tag);
    Clients.update(
        { _id: {$in: clients}},
        {$addToSet: { tags: tag } },
        {multi: true},
    );
  },
  'clients.clearTag'(tag) {
    /*check(client,{
      name : String,
      os : String,
      ip : String,
    })*/
    //check(id,String)

    Clients.update(
        { tags: tag },
        {$pull: { tags: tag } },
        {multi: true},
    );

    Meteor.call('tags.updateCount',tag,0);
  },
  'clients.searchToParams'(search){
    let searchArray=[];
    if(typeof search != "undefined"){
            searchArray.push({ name: new RegExp(  search + ".*", 'i' ) });
            searchArray.push({ os: new RegExp(  search + ".*", 'i' ) });
            searchArray.push({ aliasOs: new RegExp(  search + ".*", 'i' ) });
            searchArray.push({ ip: new RegExp(  search + ".*", 'i' ) });
            searchArray.push({ state: new RegExp(  search + ".*", 'i' ) });
            searchArray.push({ 'scan.min': new RegExp(   ".*" + search  + ".*", 'i' ) });
            searchArray.push({ 'scan.hour': new RegExp(   ".*" + search  + ".*", 'i' ) });
            searchArray.push({ 'scan.day': new RegExp(   ".*" + search  + ".*", 'i' ) });
            searchArray.push({ 'scan.month': new RegExp(   ".*" + search  + ".*", 'i' ) });
            searchArray.push({ 'scan.year': new RegExp(   ".*" + search  + ".*", 'i' ) });
            //searchArray.push({ 'scan.month': new RegExp(   ".*" + search  + ".*", 'i' ) });
            //searchArray.push({ 'scan.month': new RegExp(   ".*" + search  + ".*", 'i' ) });
            searchArray.push({ tags: new RegExp(  search + ".*", 'i' ) });
          //console.log(searchArray);
          //db.clients.aggregate([{$match:{ $or : [{name: "CentOS" },{name:/ows.*/i } ] } },{$project:{name:1,ip:1,year: { $year: "$lastUpdate" },month: { $month: "$lastUpdate" } }},{$match:{year: 2017 }},{$project:{name:1} } ])
            //searchArray.push({date: new RegExp( ".*" + search  + ".*" ,'i')  });
    }
    else{
       searchArray.push({name: new RegExp( ".*", 'i' )});
    }
    return searchArray;
  }


});


  /*function transform(search){

    // search.map( (elem,index) => {
    //   console.log(elem,index);
    //
    // })
    let open=0,close=0,expression='',expAr=[];
    for (var i = 0; i < search.length; i++) {
      if(search[i]=='(')
      {
        open++;
      }
      else if(search[i]==')')
      {
        open--;
        expAr.push(expression);
        expression='';
        //console.log(expAr);
      }else if(search[i]!=' ' && search[i]!='&' && search[i]!='|') {
        expression+=search[i];
      }
    }
  }*/

  const chartsList = Charts.find({}).fetch();
  //console.log(chartsList);

  chartsList.map((elem, index) => {
    Meteor.publish(elem.name,()=>{
      return Clients.find({name: new RegExp(  elem.search + ".*", 'i' )});
    })
  })

  Meteor.publish('clients', function clientsPublication(search,options,advancedSearch) {
    //console.log(advancedSearch);
    if(advancedSearch == true){
      //console.log(search);
      // (name=['centos','centos2']) && ((state='critical') || (ip != 192.168.1.1) )
      // { $and :[{name: {$in : ['centos','centos2']}} , {$or: [{state : 'critical'},{ ip : {$ne : '192.168.1.1' }}] } ] }
    }else {}
  //  transform(search);

    let searchArray = Meteor.call('clients.searchToParams',search);
    //let searchArray =  search ;
    //console.log(searchArray);
    let searchQuery = {};
    if(search!='')
    {
      searchQuery = compile(search);
    }

    ReactiveAggregate(this, Clients, [
      { $unwind : "$tags" },
      {"$group":{"_id":"$tags","count":{$sum:1}}}

    ], { clientCollection: "tagsAgg" });

    ReactiveAggregate(this, Clients, [
      //{ $unwind : "$tags" },
      {"$group":{"_id":"$aliasOs","count":{$sum:1}}}

    ], { clientCollection: "osAgg" });

    ReactiveAggregate(this, Clients, [
      //{ $unwind : "$tags" },
      {"$group":{"_id":"count","count":{$sum:1}}}

    ], { clientCollection: "allMachinesCount" });

    ReactiveAggregate(this, Clients, [
      //{ $unwind : "$tags" },
      {"$group":{"_id":"$state","count":{$sum:1}}}

    ], { clientCollection: "statesAgg" });
    //console.log(searchArray);
      if(advancedSearch == true){
      return Clients.find(searchQuery);
      }else {
         //console.log(Clients.find(searchQuery).fetch());
        return Clients.find({$or: searchArray}); //{$or: searchArray}
         //{$or: searchArray}
        //full text search enabled
        /*const clients = Clients.find({$text: {$search:searchArray}});
        let clientsIDs = clients.map(e => e._id);
        //console.log(clientsIDs);
        Clients.ClientsIDs.remove({});
        _.each(clientsIDs, (doc) => {
          //console.log(doc);
          Clients.ClientsIDs.insert({id:doc});
        })

        //console.log(Clients.ClientsIDs.find({}).fetch());
        clientsIDs = Clients.ClientsIDs.find({});
        //console.log(clients.map(e => e._id));
        //console.log(searchArray);
        //console.log(clients.fetch());
        return [clientsIDs,clients]; //{$or: searchArray}*/
      }
     //return result.find();
  });
}
