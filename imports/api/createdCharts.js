import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Charts = new Mongo.Collection('charts');

if (Meteor.isServer) {
  // This code only runs on the server
Meteor.methods({


  //receive request
  'charts.addChart'(chart) {
    /*check(client,{
      name : String,
      os : String,
      ip : String,
    })*/



    // Make sure the user is logged in before inserting a task
    /*if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }*/


    //console.log(chart);
    //console.log(new Date());
    const id = new Meteor.Collection.ObjectID();
    //const updte= new Date();

    Charts.insert({
      _id: id,
      name: chart.name,
      search: chart.search,
      focus: chart.focus,
      type: chart.type,

      });
      //console.log(chart);
    //return id;

  },
  'charts.deleteChart'(chartId){
    const id = new Meteor.Collection.ObjectID(chartId);
    Charts.remove({
      _id: id,
      });
  }


});


  Meteor.publish('charts', () => {
      //console.log(TagsCollection.find({}).fetch());
     return Charts.find({});
  });
}
