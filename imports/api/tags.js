import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const TagsCollection = new Mongo.Collection('tags');

if (Meteor.isServer) {
  // This code only runs on the server
Meteor.methods({


  //receive request
  'tags.addTag'(tag) {
    /*check(client,{
      name : String,
      os : String,
      ip : String,
    })*/



    // Make sure the user is logged in before inserting a task
    /*if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }*/


    //console.log(client);
    //console.log(new Date());
    const id = new Meteor.Collection.ObjectID();
    //const updte= new Date();

    TagsCollection.insert({
      _id: id,
      name: tag.name,
      type: tag.type,
      match: '',
      count: '0',

      });

    //return id;

  },
  'tags.match'(tag,count,match) {
    /*check(client,{
      name : String,
      os : String,
      ip : String,
    })*/


    // Make sure the user is logged in before inserting a task
    /*if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }*/
    //console.log(id);
    /*const clientId= new Meteor.Collection.ObjectID(id);
    //console.log(clientId);
    const updte= new Date();
    */
    TagsCollection.update({name : tag}, {$set: {
      match: match,
      count: count,
      }},
      //{upsert : true},
    );
    return count;
  },
  'tags.deleteTag'(tagName) {
    const tag = TagsCollection.findOne({name : tagName});
    if(tag.count == 0){
      TagsCollection.remove({name : tagName});
      return true;
    }else {
      return false;
    }
  },
  'tags.getDynamique'() {

    return TagsCollection.find({type : 'Dynamique'}).fetch();
  },
  'tags.updateCount'(tag,count) {
    TagsCollection.update({name : tag}, {$set: {
      count: count,
      }},
    );
  },

});


  Meteor.publish('tags', () => {
      //console.log(TagsCollection.find({}).fetch());
     return TagsCollection.find({});
  });
}
