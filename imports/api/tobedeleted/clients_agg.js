import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Clients = new Mongo.Collection('clients');

if (Meteor.isServer) {
  // This code only runs on the server
Meteor.methods({


  //receive request
  'clients.addMachine'(client) {
    check(client,{
      name : String,
      ip : String,
    })


      client.state = 'Unknown';



    // Make sure the user is logged in before inserting a task
    /*if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }*/

    const id = new Meteor.Collection.ObjectID();
    //console.log(id);
    Clients.insert({
      _id: id,
      name: client.name,
      ip: client.ip,
      state: client.state,
      lastUpdate: new Date(),
      });

    return id;

  },
  'clients.receive'(client,id) {
    check(client,{
      name : String,
      ip : String,
    })
    check(id,String)
    //console.log(client);
    switch(Math.floor((Math.random() * 10) + 1)%3) {
    case 0:
      client.state = 'Safe';
        break;
    case 1:
      client.state = 'Risky';
        break;
    default:
      client.state = 'Critical';
      }


    // Make sure the user is logged in before inserting a task
    /*if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }*/
    //console.log(id);
    const clientId= new Meteor.Collection.ObjectID(id);
    //console.log(clientId);
    Clients.update(clientId, {$set: {
      name: client.name,
      ip: client.ip,
      state: client.state,
      lastUpdate: new Date(),
      }},
      //{upsert : true},
    );
  },
  'clients.search'(search,options){

      if(search == null ){
        //let search = {};
        search='';
      }

      let searchArray=[];
      if(typeof search != "undefined"){
              searchArray.push({ name: new RegExp(  search + ".*", 'i' ) });
              searchArray.push({ ip: new RegExp(  search + ".*", 'i' ) });
              searchArray.push({ state: new RegExp(  search + ".*", 'i' ) });
              //searchArray.push({ lastUpdate: new RegExp(   ".*" + search  + ".*", 'i' ) });
              searchArray.push({ tags: new RegExp(  search + ".*", 'i' ) });
            //console.log(searchArray);
            //db.clients.aggregate([{$match:{ $or : [{name: "CentOS" },{name:/ows.*/i } ] } },{$project:{name:1,ip:1,year: { $year: "$lastUpdate" },month: { $month: "$lastUpdate" } }},{$match:{year: 2017 }},{$project:{name:1} } ])
              searchArray.push({date: new RegExp( ".*" + search  + ".*" ,'i')  });
      }
      else{
         searchArray.push({name: new RegExp( ".*", 'i' )});
      }

      count = Clients.aggregate([
        //{$match:{ $or : searchArray } },
        {$project:{_id:1,name:1,ip:1,lastUpdate:1,tags:1,state:1,date : { $dateToString: { format: "%d-%m-%Y", date: "$lastUpdate" } } }},
        {$match:{ $or : searchArray }},
        {$project:{_id:1,name:1,ip:1,lastUpdate:1,tags:1,state:1} },
        {$group: {_id : null,count : {$sum : 1}}}
      ]);
      //console.log(count[0].count);

      result = Clients.aggregate([
        //{$match:{ $or : searchArray } },
        {$project:{_id:1,name:1,ip:1,lastUpdate:1,tags:1,state:1,date : { $dateToString: { format: "%d-%m-%Y", date: "$lastUpdate" } } }},
        {$match:{ $or : searchArray }},
        {$project:{_id:1,name:1,ip:1,lastUpdate:1,tags:1,state:1} },
        {$skip : options.skip},
        {$limit : options.limit},
      ]);
      //result = Clients.find({ $or : searchArray } );//(searchArray!=null)? :{}
      //console.log(searchArray);
      //console.log(result);
      //console.log(+search);
      return [result,count];
      //return result.fetch();


  },

});


  Meteor.publish('clients', function clientsPublication(search,options) {
    console.log(search);
    let searchArray=[];
    if(typeof search != "undefined"){
            searchArray.push({ name: new RegExp(  search + ".*", 'i' ) });
            searchArray.push({ ip: new RegExp(  search + ".*", 'i' ) });
            searchArray.push({ state: new RegExp(  search + ".*", 'i' ) });
            //searchArray.push({ lastUpdate: new RegExp(   ".*" + search  + ".*", 'i' ) });
            searchArray.push({ tags: new RegExp(  search + ".*", 'i' ) });
          //console.log(searchArray);
          //db.clients.aggregate([{$match:{ $or : [{name: "CentOS" },{name:/ows.*/i } ] } },{$project:{name:1,ip:1,year: { $year: "$lastUpdate" },month: { $month: "$lastUpdate" } }},{$match:{year: 2017 }},{$project:{name:1} } ])
            searchArray.push({date: new RegExp( ".*" + search  + ".*" ,'i')  });
    }
    else{
       searchArray.push({name: new RegExp( ".*", 'i' )});
    }

    result = Clients.aggregate([
      //{$match:{ $or : searchArray } },
      {$project:{_id: "$_id", name:1,ip:1,lastUpdate:1,tags:1,state:1,date : { $dateToString: { format: "%d-%m-%Y", date: "$lastUpdate" } } }},
      {$match:{ $or : searchArray }},
      {$project:{_id: "$_id"} },
      {$skip : options.skip},
      {$limit : options.limit},
    ]);

    /*ReactiveAggregate(this, Clients, [
      {$project:{name:1,ip:1,lastUpdate:1,tags:1,state:1,date : { $dateToString: { format: "%d-%m-%Y", date: "$lastUpdate" } } }},
      {$match:{ $or : searchArray }},
      {$project:{name:1,ip:1,lastUpdate:1,tags:1,state:1} },
      //{$skip : options.skip},
      //{$limit : options.limit},
    ], { clientCollection: "clientsAgg" });*/
    //result = Clients.find({ $or : searchArray } );
    let arrayIDs= result.map(( elem,index) => {
      let oid = new Meteor.Collection.ObjectID(elem._id+'');
       //console.log(new Meteor.Collection.ObjectID(elem._id+''));
      return oid ;
    })
    console.log(arrayIDs);
    result2 = Clients.find({_id : { $in : arrayIDs} });
    //console.log(result );
    //console.log(result2.fetch() );
     return Clients.find({_id : { $in : arrayIDs} }); //{$or: searchArray}
  });
}
