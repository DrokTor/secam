import React, { Component } from 'react';
import PropTypes from 'prop-types';
//import moment from 'momentjs/moment';

// Client component - represents a single machine item
export default class TagsTree extends Component {

  constructor(props){
      super(props);

      this.state = {
        name : '',
        'type' : '',
      }
      this.handleClick = this.handleClick.bind(this);

  }

  handleClick(e){
    e.preventDefault();
    const tag = this.props.getTag(e.target.dataset.tag);
    this.setState({
      name : tag.name,
      type : tag.type,
    })
  }
  render() {
    //state= this.props.client.state;
    //moment.locale('fr');

    return (

        <ul className="col-8">
        {this.props.tags.map(( elem, index)=> {
          //console.log(elem._id._str);
          return <li key={index}>
            <span  className="tag" onClick={this.handleClick} data-tag={elem._id._str}>
              {elem.name} <span className="badge badge-info">9</span>
            </span>
              </li>;
        })}
        </ul>

    );
  }
}

TagsTree.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  //client: PropTypes.object.isRequired,
};
