import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Assets from 'meteor/meteor';
import Chart from 'chart.js';
import palette from 'google-palette';
import alertify from 'alertify.js';

export default class ChartComponent extends Component {

  constructor(props){
    super(props);
    //this.handleChartClick = this.handleChartClick.bind(this);
    this.handleChartClick = this.handleChartClick.bind(this);
    this.deleteChart = this.deleteChart.bind(this);
  }
  drawCharts(){
    //console.log(this.props.chart.chart);
     labels = _.keys(this.props.chart.chart);
     counts = _.values(this.props.chart.chart);
    if(typeof chart != 'undefined' && typeof chart[this.props.refrence] != 'undefined') chart[this.props.refrence].destroy();
    const elem = ReactDOM.findDOMNode(this.refs[this.props.refrence]);
    //elem.getContext('2d').clearRect(0, 0, elem.width, elem.height);
     //elem = document.getElementById("state");
     //console.log(this.props.chart);

     let ColorScheme;
     console.log(this.props.chart.focus);
     if(this.props.chart.focus != "state"){

        ColorScheme = this.generateColors(labels.length);
     }else {
        ColorScheme = [];
       labels.map( (elem)=> {
         switch (elem) {
           case 'Safe':
             ColorScheme.push('#1eda69');
             break;
           case 'Risky':
             ColorScheme.push('#ffbd29');
             break;
           case 'Critical':
             ColorScheme.push('#ff4545');
             break;
           case 'Unknown':
             ColorScheme.push('#a4a4a4');
             break;

         }
       })

       //console.log(StateScheme);
     }
    //console.log(seq);

    data = {
        datasets: [{
            data: counts,
            //backgroundColor: ColorScheme,
            backgroundColor: (this.props.chart.focus != "sstate")? ColorScheme:StateScheme,
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: labels,
    };

    options = {
      //onClick : this.props.handleChartClick,
      legend : {
        position : (this.props.chart.type=='bar')? false:'right',
      }
    }

    if(typeof chart === 'undefined')
    {
      chart = [];
      //console.log(typeof chart);
    }
    //console.log(this.props.chart.type);
     chart[this.props.refrence] = new Chart(elem.getContext("2d"),{
    type: this.props.chart.type || 'doughnut',
    data: data,
    options: options,
    });

  }

  componentDidMount(){
    this.drawCharts();
  }

  componentDidUpdate(){
    this.drawCharts();
  }

  generateColors(length){
    const color = (length>6)? 'tol-rainbow':'cb-Set2';
    seq = palette(color, length);
    ColorScheme = seq.map((hex) => {
      return '#'+hex;
    });
    return ColorScheme;
  }

  handleChartClick(event, array){
    //console.log(array[0]['_chart']);
    if(array[0]){
      let activePoints = array[0]['_chart'].getElementsAtEvent(event);
      let chartData = activePoints[0]['_chart'].config.data;
      let idx = activePoints[0]['_index'];
      let label = chartData.labels[idx];
      let value = chartData.datasets[0].data[idx];
      //console.log(label);
      this.props.chartClicked(label);
    }
  }

  deleteChart(e){
    const id = e.target.dataset.id;
    alertify.confirm("êtes-vous sûr de vouloir supprimer ce graph ?",() => {
      Meteor.call('charts.deleteChart',id,()=>{
        alertify.success('Le graph vient d\'être supprimé.');
      });
    }, () =>{
      alertify.log('action annulé.');
    });
  }

  expandDashboard(){
      $('#dashboard-interface').toggleClass('fullscreen');
  }

  render(){
      return(
              <div className="mb-40 mr-auto col-6">
                <label className="col-12">{this.props.chart.name}<i className="fa fa-trash float-right pointer" data-id={this.props.chart.id} onClick={this.deleteChart} ></i></label>
               <canvas id={this.props.id} ref={this.props.refrence} width="400" height="200"></canvas>
              </div>
            );
  }

}
