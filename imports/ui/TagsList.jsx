import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Session } from 'meteor/session';
import alertify from 'alertify.js';

import TagDetails from './TagDetails.jsx';
import Search from './Search.jsx';
//import moment from 'momentjs/moment';

// Client component - represents a single machine item
export default class TagsList extends Component {

  constructor(props){
      super(props);

      this.state = {
        tag: {},
        selected: null,
        //search : false,
      }
      this.handleClick = this.handleClick.bind(this);
      this.assignInterface = this.assignInterface.bind(this);
      this.assign = this.assign.bind(this);
  }

  handleClick(e){
    e.preventDefault();
    const tag = this.props.getTag(e.target.dataset.tag || e.target.parentNode.dataset.tag);
    //console.log(tag);
    $('.tag.selected').removeClass('selected');
    $(e.target).hasClass('tag')?
      $(e.target).addClass('selected')
      :$(e.target.parentNode).addClass('selected');

    this.setState({
      tag : tag,
      selected: e.target.id,
    },()=>{
    //  console.log(this.state.tag);
    })
  }
  assignInterface(){
    //e.preventDefault();
    this.props.assignInterface();
  }
  assign(){
    //e.preventDefault();
      this.props.assign(this.state.tag.name,()=>{
        $('#'+this.state.selected).click();
      });
  }
  render() {
    //state= this.props.client.state;
    //moment.locale('fr');

    return (
      <div className="row">
        {(this.props.searchState != true)?
        <ul className="col-8">
        {this.props.tags.map(( elem, index)=> {
          //console.log(elem._id._str);
          return <li key={index}>
            <span  id={elem._id._str} className="tag" onClick={this.handleClick} data-tag={elem._id._str}>
              {elem.name} <span className="badge badge-info">{elem.count}</span>
            </span>
              </li>;
        })}
        </ul>
        :
        <div className=" col-8">
          <div className="tag-search">
          <Search search={this.props.search} text={Session.get('search') || ''} tagsection={true} />
          </div>
          <div className="row">
            <label className="offset-1 col-4" >Machines trouvées:</label><div className="col-5">{this.props.count}</div>
            <button className="btn btn-success " onClick={this.assign} >Procéder > </button>
          </div>
        </div>
        }
        <div className="col-4 tag-info">
          <TagDetails tagsList={this.props.tags} tag={this.state.tag}  assignInterface={this.assignInterface} searchTag={this.props.searchTag}/>
        </div>
      </div>
    );
  }
}

TagsList.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  //client: PropTypes.object.isRequired,
};
