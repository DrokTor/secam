import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';
//import { Clients } from '../api/clients.js';

import App from './App.jsx';
import Login from './Login.jsx';

// App component - represents the whole app
export  class AppInit extends Component {

  render() {


    return (
      <div className="col-10 offset-1">

      <div className=" col-md-12" >

        {( () => {
          return (this.props.user) ?  <App /> : <Login />;
        })()}

      </div>
      </div>
    );
  }


}

export default withTracker( () => {
  const user=Meteor.userId();

  return {
    user: user,
  };
}
)(AppInit);
