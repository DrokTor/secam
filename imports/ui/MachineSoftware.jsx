import React, { Component } from 'react';
import PropTypes from 'prop-types';
//import moment from 'momentjs/moment';

// Client component - represents a single machine item
export default class MachineSoftware extends Component {

  constructor(props){
      super(props);

      this.searchTag = this.searchTag.bind(this);
  }

  searchTag(e){
    this.props.searchTag(e.target.dataset.tagname);
  }
  render() {
    moment.locale('fr');
    //console.log(this.props.machine);
    const software = this.props.software || [];
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col">
            <table>
              <thead>
              <tr><th>Nom</th><th>Éditeur</th><th>Installation</th></tr>
              </thead>
              <tbody>
              {
                software.map((elem,index)=> {
                  return <tr key={index}><td>{elem.DisplayName}</td><td>{elem.Publisher}</td><td>{elem.InstallDate}</td></tr>;
                })
              }
              </tbody>
            </table>
          </div>

        </div>


      </div>
    );
  }
}

// Client.propTypes = {
//   // This component gets the task to display through a React prop.
//   // We can use propTypes to indicate it is required
//   //client: PropTypes.object.isRequired,
// };
