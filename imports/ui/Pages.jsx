import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';

// Client component - represents a single machine item
export default class Pages extends Component {

  constructor(props){
    super(props);
    //console.log(this.props.patient);

      this.state = {
        lastPage: null,

      };

    //console.log(this.state);
    this.handleClick=this.handleClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    //(this.state.lastPage != null)?  ReactDOM.findDOMNode(this.state.lastPage).classList.remove("current-page") : '';
    const elem = (document.getElementsByClassName("current-page")[0]);
    //console.log(elem);
    if(elem) elem.classList.remove("current-page");
    event.target.className += " current-page";
    //this.setState({lastPage: 'page'+event.target.dataset.value});
    //console.log(this.state.lastPage);
    this.props.load(event.target.dataset.value);
  }

  render() {
    //state= this.props.patient.state;
    moment.locale('fr');
    //console.log(this.props.selected==13);
    let pages = [];//new array(3);

    for (let i = 1; i <= this.props.pages; i++) {
        pages.push(<span className={(this.props.selected==i)? "page current-page" : "page"} key={i} onClick={this.handleClick} data-value={i} ref={'page'+i} > {i}</span>);
    }
     return <div>{pages}</div>;
      /*return (<div>
       <nav aria-label="Page navigation example">
         <ul className="pagination">
           <li className="page-item"><a className="page-link" href="#">Previous</a></li>
           {pages}
           <li className="page-item"><a className="page-link" href="#">Next</a></li>
         </ul>
       </nav>
     </div>);*/ 


  }
}
