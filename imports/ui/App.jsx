import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';
import { Session } from 'meteor/session';
import { HTTP } from 'meteor/http';
import { Clients } from '../api/clients.js';
import { MachineMenus } from '../api/machineMenus.js';
import { Charts } from '../api/createdCharts.js';

import compile from 'monquery';

import alertify from 'alertify.js';

import Header from './Header.jsx';
import Dashboard from './Dashboard.jsx';
import Client from './Client.jsx';
//import ClientsIDs from './Client.jsx';
import Tags from './Tags.jsx';
import Search from './Search.jsx';
import MachineDetails from './MachineDetails.jsx';
import Pages from './Pages.jsx';

export const StatesAgg = new Meteor.Collection('statesAgg');
export const TagsAgg = new Meteor.Collection('tagsAgg');
export const OsAgg = new Meteor.Collection('osAgg');
export const allMachinesCount = new Meteor.Collection('allMachinesCount');

// App component - represents the whole app
class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      renderPage : 'dashboard',
      scrollDown : false,
      MachineDetails : false,
      ClientsReady : 'false',
      //ClientsSearch : [{name: new RegExp( ".*", 'i' )}],
    };
    this.renderPage = this.renderPage.bind(this);
    this.loadPage = this.loadPage.bind(this);
    this.showMachine = this.showMachine.bind(this);
    this.backToClients = this.backToClients.bind(this);
    this.searchMachine = this.searchMachine.bind(this);
    this.handleChartClick = this.handleChartClick.bind(this);
    this.assignTags = this.assignTags.bind(this);
    this.searchTag = this.searchTag.bind(this);
    this.showDashboard = this.showDashboard.bind(this);

  }

  renderPage(page) {
    if(this.state.renderPage == 'clients' && page == 'clients')
    {
      this.backToClients();
    }else {
      this.setState({
        renderPage : page,
      });
    }
  }

  renderClients() {
    //Session.set( "machinesCount",this.props.machinesCount )
    return this.props.clients.map((client) => ( // reactive array package
      <Client key={client._id._str} client={client} more={this.showMachine} />
     ));
  }

  loadPage(page){
    Session.set( "loadPage", page );
  }

  searchMachine(search){
    //Session.set( "searchMachine", search );
    //Session.set( "machinesCount",this.props.machinesCount );
    //console.log(this.props.machinesCount);
    Session.set( "loadPage", 1 );


    //Session.set( "searchClients",searchArray );
    Session.set( "search",search.searchText );
    Session.set( "advancedSearch",search.advancedSearch );
  }

  showMachine(client){
    //$('.slider-left').removeClass('animated sld-Right');
    //$('.slider-right').addClass('animated sld-Left');
    //$('.slider-left').css('transform', 'scale(0.8)');
    $('.slider-left').fadeOut('fast',()=>{
      $('.slider-right').fadeIn();
    });
    //console.log(client);
    this.setState({
      MachineDetails : client,
    });
  }
  backToClients(){
    //$('.main-slider').removeClass('animated sld-Left');
    //$('.main-slider').addClass('animated sld-Right');
    $('.slider-right').fadeOut('fast',()=>{
      $('.slider-left').fadeIn();
      this.setState({
        MachineDetails : false,
      });
    });

  }

  handleChartClick(label){
      //alert(label);
      Session.set('search',label);
      this.renderPage('clients');
  }
  assignTags(name,callback){
    alertify.confirm("êtes-vous sûr de vouloir affecter ce tag ?",() => {
      Meteor.call('clients.addTag',_.pluck(this.props.allClients,'_id'),name, ()=>{
        //this.renderPage('clients');
        Meteor.call('tags.match',name,this.props.allClients.length,Session.get('search') || '',(error,result)=>{
          alertify.success('Le tag vient d\'être affecté.');
          callback();
        });
      });
    }, () =>{
      alertify.log('action annulé.');
    });
  }
  searchTag(match){
    Session.set('search',match);
    this.renderPage('clients');
  }
  showDashboard(){
    this.setState({scrollDown:true});
    this.renderPage('dashboard');
  }


  render() {

    return (
      <div className="">
        <Header onMenuLink={this.renderPage}/>

      <div className="ov-x-h padding-0 col-md-12" >

        {( () => {
          //console.log(this.props.critical);
          switch (this.state.renderPage) {

            case 'dashboard':
              return(
                <Dashboard chartClicked={this.handleChartClick} statesCount={this.props.statesCount} osCount={this.props.osCount} tagsCount={this.props.tagsCount} clientsCharts={this.props.clientsCharts} total={this.props.allMachinesCount} scrollDown={this.state.scrollDown}/*critical={this.props.critical} risky={this.props.risky} safe={this.props.safe} unknown={this.props.unknown}*/ />
                );
            case 'clients':
              return (
                <div className="main-slider">
                  <div className="slider slider-left" >
                    <Search /*searchParams={Session.get("searchMachine")  }*/ total={this.props.machinesCount} search={this.searchMachine} text={Session.get('search') || ''} showDashboard={this.showDashboard}/>
                    <table className="col-md-12" >
                      <thead>
                        <tr className="row" ><th className="col-2">Nom du PC</th><th className="col-2">OS</th><th className="col-2">IP</th><th className="col-3">Scan</th><th className="col-2">Tags</th><th className="col-1">Etat</th></tr>
                      </thead>
                      <tbody>
                        {this.renderClients()}
                      </tbody>
                    </table>
                  </div>
                  {(!this.state.MachineDetails)?
                  <div className= "pages col-6 text-center  ">
                    <Pages selected={Session.get("loadPage") || 1} pages={Math.ceil(this.props.machinesCount/this.props.perPage)} load={this.loadPage}/>
                  </div>
                  :''}

                  <div className="slider slider-right slider-hidden ov-h" >
                    <h5 className="offset-md" >Détails de la machine<span  onClick={this.backToClients} className="pointer float-md-right" >  X </span></h5>
                    {(this.state.MachineDetails)?
                    <MachineDetails menu={this.props.machineMenus}  machine={this.state.MachineDetails} searchTag={this.searchTag}/>
                      : ''}
                  </div>
                </div>

              );

              case 'tags':
                return(
                  <div className="" >
                  <Tags search={this.searchMachine} count={this.props.machinesCount} assign={this.assignTags} searchTag={this.searchTag} />
                  </div>
                  );
          }
        })()}

      </div>
      </div>
    );
  }
}


App.propTypes = {
  clients : PropTypes.array.isRequired,
};


export default withTracker((state) => {

  const searchToParams = (search) => {
    let searchArray=[];
    if(typeof search != "undefined"){
            searchArray.push({ name: new RegExp(  search + ".*", 'i' ) });
            searchArray.push({ os: new RegExp(  search + ".*", 'i' ) });
            searchArray.push({ aliasOs: new RegExp(  search + ".*", 'i' ) });
            searchArray.push({ ip: new RegExp(  search + ".*", 'i' ) });
            searchArray.push({ state: new RegExp(  search + ".*", 'i' ) });
            searchArray.push({ 'scan.min': new RegExp(   ".*" + search  + ".*", 'i' ) });
            searchArray.push({ 'scan.hour': new RegExp(   ".*" + search  + ".*", 'i' ) });
            searchArray.push({ 'scan.day': new RegExp(   ".*" + search  + ".*", 'i' ) });
            searchArray.push({ 'scan.month': new RegExp(   ".*" + search  + ".*", 'i' ) });
            searchArray.push({ 'scan.year': new RegExp(   ".*" + search  + ".*", 'i' ) });
            //searchArray.push({ 'scan.month': new RegExp(   ".*" + search  + ".*", 'i' ) });
            //searchArray.push({ 'scan.month': new RegExp(   ".*" + search  + ".*", 'i' ) });
            searchArray.push({ tags: new RegExp(  search + ".*", 'i' ) });
          //console.log(searchArray);
          //db.clients.aggregate([{$match:{ $or : [{name: "CentOS" },{name:/ows.*/i } ] } },{$project:{name:1,ip:1,year: { $year: "$lastUpdate" },month: { $month: "$lastUpdate" } }},{$match:{year: 2017 }},{$project:{name:1} } ])
            //searchArray.push({date: new RegExp( ".*" + search  + ".*" ,'i')  });
    }
    else{
       searchArray.push({name: new RegExp( ".*", 'i' )});
    }
    return searchArray;
  }

  const page=Session.get( "loadPage" ) || 0;
  const perPage=10;
  const searchParams=Session.get( "searchMachine" ) || {};



  const MAX = 10;
  const options = {
      sort: {createdAt: 1},
      limit: Math.min(perPage, MAX),
      skip: (page > 0 ? ((page-1)*perPage) : 0),
    };
  //machineMenus = MachineMenus.find({}).fetch();
  //console.log(machineMenus);
  //(Clients.find({}).count())?Session.set( "machinesCount",this.props.machinesCount ):'';

  Meteor.subscribe('clients',Session.get('search') || '',options, Session.get('advancedSearch') || ''); //,options

  //console.log(Clients[0].find({}));
  //Clients = new Mongo.Collection('clients');
  Meteor.subscribe('machineMenus');
  Meteor.subscribe('charts');

  const chartsList = Charts.find({}).fetch();
    //console.log(chartsList);
    let chartsArray = [];
    chartsList.map((elem, index) => {
      Meteor.subscribe(elem.name);
      const clientsChart = Clients.find({$or:searchToParams(elem.search)}).fetch();
      const focus = elem.focus || 'state';
      /*if(focus == 'state'){

      const statecharts = _.chain(clientsChart).countBy(elem => elem.state).pairs().sortBy(
        (elem)=>{
          switch (elem[0]) {
            case 'Safe':
              return 1;
              break;
            case 'Risky':
              return 2;
              break;
            case 'Critical':
              return 3;
              break;
            case 'Unknown':
              return 4;
              break;
            default:

          }
        }
      ).value();
      //console.log(statecharts);
    }*/
      //console.log(statecharts  );
      //console.log(_.countBy(clientsChart,(elem)=>{ return elem.state }));
      //chartsArray.push(clientsChart);
      //console.log(_.chain(clientsChart).map(d=>d.tags).flatten().countBy().value());
      if(focus == 'state' && typeof(statecharts)!= 'undefined') console.log(statecharts);
      chartsArray.push({
          id:elem._id._str,
          name:elem.name,
          type:elem.type,
          focus:focus,
          chart:(()=>{if(focus == 'tag'){
                  return _.chain(clientsChart).map(machine=>machine.tags).flatten().countBy().value()
                }else if(focus == 'state' ){
                    return _.chain(clientsChart).countBy(elem => elem.state).pairs().sortBy(
                      (elem)=>{
                        switch (elem[0]) {
                          case 'Safe':
                            return 1;
                            break;
                          case 'Risky':
                            return 2;
                            break;
                          case 'Critical':
                            return 3;
                            break;
                          case 'Unknown':
                            return 4;
                            break;
                          default:

                        }
                      }
                    ).object().value();
                  }else{
                    return _.countBy(clientsChart,elem=> elem.os)
                  }})()

          });
    });

    //Session.set('chartsArray',chartsArray);
    //console.log(chartsArray);
    let searchQuery = {};
    if(Session.get('search') && Session.get('search').trim()!='' && Session.get('advancedSearch'))
    {
      try {
        let replace = Session.get('search');
        // compile is a package to translate query to mongodb query
        searchQuery = compile(replace);
        //console.log(searchQuery);

      } catch (e) {
        //console.log(e.message);
      } finally {

      }
    }

    let searchArray = [{name: new RegExp( ".*", 'i' )}];
    if(!Session.get('advancedSearch'))
    {
      searchArray = searchToParams(Session.get('search'));
      //console.log(searchArray);
    }
  //let searchArray = [{name: new RegExp( ".*", 'i' )}];
  //searchArray = searchToParams(Session.get('search'));
  //let searchArray =  Session.get('search');
  //console.log(searchToParams(Session.get('search')));
  //console.log(Clients.find({}).fetch());
  //console.log(Clients.find({name: new RegExp(  'ubuntu 14' + ".*", 'i' )}).fetch());
  /*
  Meteor.call('clients.search',Session.get('search'),options,(error,result)=>{
    clients = result;
    //Session.set( "searchClients",result );
    //console.log(Session.get( "searchClients" ));
    //console.log(result);
    Session.set( "clients",result[0] );
    Session.set( "clientsCount",result[1][0].count );
  })*/
  //console.log(Clients.find({},options).fetch());
  //clients = Session.get( "searchClients")? clients : [];
  //console.log(Session.get( "searchClients")? 1:0);
  //console.log(Session.get( "clients" ));
  //console.log(Session.get( "clientsCount" ));
  //console.log(Session.get( "clientsCount" ));
  //console.log(Clients.ClientsIDs.find().fetch());
  const clientsIDs = _.pluck(Clients.ClientsIDs.find().fetch(),'id');
  //console.log(clientsIDs);
  //console.log(MachineMenus.find({}).fetch());
  const total = allMachinesCount.find({}).fetch()[0] || 0;
  //console.log(StatesAgg.find({}).fetch());
  let sortedStates= _.sortBy(StatesAgg.find({}).fetch(),(elem)=> {
    switch (elem._id) {
      case 'Safe':
        return 1;
        break;
      case 'Risky':
        return 2;
        break;
      case 'Critical':
        return 3;
        break;
      case 'Unknown':
        return 4;
        break;
      default:

    }
  })
  //console.log(sortedStates);
  //sortedStates = [{_id:"Risky",count:20},{_id:"Unknown",count:26}]
  return {
    perPage,
    //clients:Session.get( "searchClients"),
    //clients: Clients.find({$or:searchArray},options).fetch(),//(typeof Session.get( "clients" ) != 'undefined')? Session.get( "clients" ):[],//Clients.find({},options).fetch(),
    clients: Clients.find(Session.get('advancedSearch')? searchQuery:{$or:searchArray},options).fetch(),//(typeof Session.get( "clients" ) != 'undefined')? Session.get( "clients" ):[],//Clients.find({},options).fetch(),
    //clients: Clients.find({_id : {$in: clientsIDs} },options).fetch(),//(typeof Session.get( "clients" ) != 'undefined')? Session.get( "clients" ):[],//Clients.find({},options).fetch(),
    statesCount : sortedStates,//StatesAgg.find({}).fetch(),
    tagsCount : TagsAgg.find({}).fetch(),
    osCount : OsAgg.find({}).fetch(),
    allMachinesCount : total.count,
    clientsCharts : chartsArray,
    //critical: Clients.find({'state' : 'Critical'}).count(),
    //risky: Clients.find({'state' : 'Risky'}).count(),
    //safe: Clients.find({'state' : 'Safe'}).count(),
    //unknown: Clients.find({'state' : 'Unknown'}).count(),
    //allClients: Clients.find({$or:searchArray}).fetch(),
    allClients: Clients.find(searchQuery).fetch(),
    //allClients: Clients.find({_id : {$in: clientsIDs} }).fetch(),
    machinesCount:  Clients.find(Session.get('advancedSearch')? searchQuery:{$or:searchArray}).count(),//(typeof Session.get( "clientsCount" ) != 'undefined') ? Session.get( "clientsCount" ) : 0,//Clients.find({}).count(),
    //machinesCount: clientsIDs.length,//  Clients.find({$or:searchArray}).count(),//(typeof Session.get( "clientsCount" ) != 'undefined') ? Session.get( "clientsCount" ) : 0,//Clients.find({}).count(),
    machineMenus: MachineMenus.find({}).fetch(),
  };
})(App);
