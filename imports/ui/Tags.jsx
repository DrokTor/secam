import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';
import { Session } from 'meteor/session';

import TagsList from './TagsList.jsx';
import TagsNew from './TagsNew.jsx';
//import moment from 'momentjs/moment';
import { TagsCollection } from '../api/tags.js';
//import { Clients } from '../api/clients.js';
// Client component - represents a single machine item


export  class Tags extends Component {

  constructor(props){
    super(props);
    this.state = {
      interface : 'TagsList',
      search : false,
    };

    this.handleClick = this.handleClick.bind(this);
    this.afterAdd = this.afterAdd.bind(this);
    this.getTag = this.getTag.bind(this);
    this.assignInterface = this.assignInterface.bind(this);
    this.assign = this.assign.bind(this);
  }
  handleClick(event){
    const int = event.target.dataset.interface;
    //alert(int);
    this.setState({interface : int,search : false});

  }
  afterAdd(){
    this.setState({interface : 'TagsList'});
  }
  assignInterface(name){
    this.setState({search : true});
  }
  assign(name,callback){
    //this.setState({search : true});
    this.props.assign(name,()=>{
      this.setState({search : false},()=>{
        callback();
      });
    });
  }

  getTag(str){
    const tag = this.props.tags.find( (tag) => { return tag._id._str == str; } );
    Session.set('tagID',str);
    //console.log(this.props.tagSelected);
    return tag;
  }
  render() {
    //state= this.props.client.state;
    //moment.locale('fr');

    return (
      <div>
        <h5 className="" >Tags <div className="col-2 float-right"><i className="offset-5  col-3  fa fa-list pointer " data-interface="TagsList" onClick={this.handleClick} ></i><i className="offset-1 col-3  fa fa-plus pointer " data-interface="TagsNew" onClick={this.handleClick} ></i></div></h5>
        {(this.state.interface=='TagsList')?
          <TagsList search={this.props.search} count={this.props.count} assign={this.assign} assignInterface={this.assignInterface} searchState={this.state.search}  tags={this.props.tags} getTag={this.getTag} searchTag={this.props.searchTag} /> :
           <TagsNew afterAdd={this.afterAdd}/>}
      </div>
    );
  }
}

Tags.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  //client: PropTypes.object.isRequired,
};
export default withTracker(() => {

  Meteor.subscribe('tags');
   //console.log(Session.get('tagID'));
   //console.log(TagsCollection.find({'_id': new Meteor.Collection.ObjectID(Session.get('tagID'))}).fetch());
  return {
    tags: TagsCollection.find({}).fetch(),
    tagSelected: TagsCollection.find({'_id': new Meteor.Collection.ObjectID(Session.get('tagID'))}).fetch(),
  }

})(Tags);
