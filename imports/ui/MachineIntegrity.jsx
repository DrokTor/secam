import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
//import moment from 'momentjs/moment';
import alertify from 'alertify.js';
// Client component - represents a single machine item
export default class MachineIntegrity extends Component {

  constructor(props){
      super(props);
      this.createIntegrityCheck = this.createIntegrityCheck.bind(this);
  }

  createIntegrityCheck(event) {
    //event.preventDefault();
    const file = ReactDOM.findDOMNode(this.refs['filePath']).value;


      //const control = { name : file }
      Meteor.call('integrity.addControl',file,()=>{
        alertify.success('Item rajouté avec succès:' + file);
        //this.props.showDashboard();
      });
  }

  cancelCreateIntegrityCheck(){
    alertify.error('Action annulée');
  }

  render() {
    moment.locale('fr');
    //console.log(this.props.integrity);
    return (
      <div className="container-fluid">
        <div className="row mb-3" >
          <div className="col-12">
            <button className="btn    float-right" data-toggle="modal" data-target="#integrityModal"><i className="fa fa-plus" ></i></button>
          </div>
        </div>
        <div className="row">
          <div className="container-fluid ov-y-a integritySection">
           {this.props.integrity.map((file,index)=>{
             return(
              <div key={index} className="row form-group">
               <div className="col-6">
                 <label>Fichier:</label><br/>
               <input
                 type="text"
                 value={file.path}
                  className="col-12"
               />
              </div>
              <div className="col-6">
               <label>Hash:</label><br/>
               <p className="col-12">
                 <div className="row">{file.hash}</div>
               </p>
             </div><hr/>
             </div>
           )
           })}

          </div>

        </div>

        {/* add file check hash info modal */}
         <div id="integrityModal" className="modal fade" role="dialog">
           <div className="modal-dialog">
             <div className="modal-content">
               <div className="modal-header">
                 Ajouter un nouveau contrôle.
                 <button type="button" className="close " data-dismiss="modal">&times;</button>
               </div>
               <div className="modal-body">
                 <div className="form-group">
                   <label>Chemin du ficher</label><br/>
                   <input
                     type="text"
                     ref="filePath"
                   />
                 </div>
               </div>
               <div className="modal-footer">
                 <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.createIntegrityCheck} >Ajouter</button>
                 <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.cancelCreateIntegrityCheck} >Annuler</button>
               </div>
             </div>

           </div>
         </div>
      </div>
    );
  }
}

// Client.propTypes = {
//   // This component gets the task to display through a React prop.
//   // We can use propTypes to indicate it is required
//   //client: PropTypes.object.isRequired,
// };
