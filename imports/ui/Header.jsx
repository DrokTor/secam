import React, { Component } from 'react';
import Assets from 'meteor/meteor';
import AccountsUIWrapper from './AccountsUIWrapper.jsx';


export default class Header extends Component {

  renderPage(e){
    this.props.onMenuLink(e.target.dataset.page);
  }

  render(){

      return(
        <header className="container-fluid " >
          <div id="logo" className="d-inline-block col-6">
            <img src=  "/assets/images/logo.svg" width="70"  />
          </div>
          <div className="d-inline-block col-6 ">
            <div className="col-8 d-inline-block ">
              <nav className="row align-items-end">
                <a onClick={this.renderPage.bind(this)} data-page="dashboard" type="button" className="btn btn-light" >Dashboard</a>
                <a onClick={this.renderPage.bind(this)} data-page="clients" type="button" className="btn btn-light" >Cloud Agents</a>
                <a onClick={this.renderPage.bind(this)} data-page="tags" type="button" className="btn btn-light" >Tags</a>
              </nav>
            </div>
            <div className="col-3 d-inline-block offset-1" >
            <AccountsUIWrapper  />
            </div>
          </div>
        </header>

      );
  }

}
