import React, { Component } from 'react';
import PropTypes from 'prop-types';
//import moment from 'momentjs/moment';

// Client component - represents a single machine item
export default class TagsNew extends Component {

  constructor(props){
    super(props);

    this.state = {
      name:'',
      type:'Statique',
      //tagParentID:'',
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e){
    e.preventDefault();
    //console.log(this.state);
    Meteor.call('tags.addTag',this.state,()=>{
      this.props.afterAdd();
    });
    //this.props.more(this.props.client);
  }

  render() {
    //state= this.props.client.state;
    //moment.locale('fr');

    return (
      <div>
        <form className="offset-3 col-6 pt-3">
            <div className="form-group">
            <input
              className="col-12 "
              type='text'
              ref='tagName'
              value={this.state.name}
              onChange={ (e) => this.setState({ name: e.target.value }) }
              placeholder="Nom du tag"
            />
            </div>
            <div className="form-group">
            <select
              className="col-12 "
              ref="tagType"
              value={this.state.type}
              placeholder="Type du tag"
              onChange={ (e) => this.setState({ type: e.target.value }) }
              >
              <option disabled>Type</option>
              <option>Statique</option>
              <option>Dynamique</option>
            </select>
            </div>
            <button className="col-12 btn btn-primary" onClick={this.handleClick}>Ajouter</button>
        </form>
      </div>
    );
  }
}

TagsNew.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  //client: PropTypes.object.isRequired,
};
