import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Assets from 'meteor/meteor';
import Chart from 'chart.js';
import palette from 'google-palette';

import ChartComponent from './Chart.jsx' ;

export default class Dashboard extends Component {

  constructor(props){
    super(props);
    //this.handleChartClick = this.handleChartClick.bind(this);
    this.handleChartClick = this.handleChartClick.bind(this);
  }
  drawCharts(){
    //console.log(this.props.tagsCount);
    //console.log(_.pluck(this.props.tagsCount,'_id'));
    //console.log(this.props.clientsCharts);

     statesLabels = _.pluck(this.props.statesCount,'_id');
     statesCounts = _.pluck(this.props.statesCount,'count');
     tagsLabels = _.pluck(this.props.tagsCount,'_id');
     tagsCounts = _.pluck(this.props.tagsCount,'count');
     osLabels = _.pluck(this.props.osCount,'_id');
     osCounts = _.pluck(this.props.osCount,'count');
    if(typeof criticityChart != 'undefined') criticityChart.destroy();
    if(typeof tagsChart != 'undefined') tagsChart.destroy();
    if(typeof osChart != 'undefined') osChart.destroy();
    const criticityElem = ReactDOM.findDOMNode(this.refs["state"]);
    const tagsElem = ReactDOM.findDOMNode(this.refs["tags"]);
    const osElem = ReactDOM.findDOMNode(this.refs["os"]);
    //elem.getContext('2d').clearRect(0, 0, elem.width, elem.height);
     //elem = document.getElementById("state");

    const stateBackgroundColor = [];
    statesLabels.map( (elem)=> {
      switch (elem) {
        case 'Safe':
          stateBackgroundColor.push('#1eda69');
          break;
        case 'Risky':
          stateBackgroundColor.push('#ffbd29');
          break;
        case 'Critical':
          stateBackgroundColor.push('#ff4545');
          break;
        case 'Unknown':
          stateBackgroundColor.push('#a4a4a4');
          break;

      }
    })
    data = {
        datasets: [{
            data: statesCounts,
            backgroundColor: stateBackgroundColor,/*[
                    '#1eda69',
                    '#ffbd29',
                    '#ff4545',
                    '#a4a4a4',
                ],*/
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: statesLabels,
    };

    ColorScheme = this.generateColors(tagsLabels.length);

    dataTags = {
        datasets: [{
            data: tagsCounts,
            backgroundColor: ColorScheme,
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: tagsLabels,
    };



    ColorScheme = this.generateColors(osLabels.length);
    //console.log(seq);

    dataOs = {
        datasets: [{
            data: osCounts,
            backgroundColor: ColorScheme,
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: osLabels,
    };

    options = {
      onClick : this.handleChartClick,
      legend : {
        position : 'right',
      }
    }

     criticityChart = new Chart(criticityElem.getContext("2d"),{
    type: 'bar',
    data: data,
    options: {
      onClick : this.handleChartClick,
      legend : {
        display : false,
      }
    },
    });

     tagsChart = new Chart(tagsElem.getContext("2d"),{
    type: 'pie',
    data: dataTags,
    options: options,
    });

     osChart = new Chart(osElem.getContext("2d"),{
    type: 'doughnut',
    data: dataOs,
    options: options,
    });


  }

  componentDidMount(){
    this.drawCharts();
    //window.scrollTo(0,document.body.scrollHeight);
  }

  componentDidUpdate(){
    this.drawCharts();
    this.props.scrollDown?window.scrollTo(0,document.body.scrollHeight):'';
  }

  generateColors(length){
    const color = (length>6)? 'tol-rainbow':'cb-Set2';
    seq = palette(color, length);
    ColorScheme = seq.map((hex) => {
      return '#'+hex;
    });
    return ColorScheme;
  }

  handleChartClick(event, array){
    //console.log(array[0]['_chart']);
    if(array[0]){
      let activePoints = array[0]['_chart'].getElementsAtEvent(event);
      let chartData = activePoints[0]['_chart'].config.data;
      let idx = activePoints[0]['_index'];
      let label = chartData.labels[idx];
      let value = chartData.datasets[0].data[idx];
      //console.log(label);
      this.props.chartClicked(label);
    }
  }

  expandDashboard(){
      $('#dashboard-interface').toggleClass('fullscreen');
  }

  render(){
      return(
        <div id="dashboard-interface" className="" >
            <h5 className="" >Dashboard <i className="fa fa-expand float-right pointer" onClick={this.expandDashboard}></i></h5>
              <div className="row mb-40" >
               <div className="ml-auto mr-auto col-6 ">
                 <label>Total des agents</label>
                <div id="total" ref="total" className="total-agents text-center" >{this.props.total}</div>
               </div>
               <div className="ml-auto mr-auto col-6">
                 <label>État global</label>
                <canvas id="state" ref="state" width="400" height="200"></canvas>
               </div>
              </div>
             <div className="row mb-40" >
               <div className="mr-auto col-6">
                 <label>Tags</label>
                <canvas id="tags" ref="tags" width="400" height="200"></canvas>
               </div>
               <div className="mr-auto col-6">
                 <label>Systèmes d'exploitations</label>
                <canvas id="os" ref="os" width="400" height="200"></canvas>
               </div>
            </div>
            <div id="createdCharts" className="row" >
              {this.props.clientsCharts.map((elem,index) => {
                return <ChartComponent id={'chart-'+index} refrence={'chart-'+index} key={'chart-'+index} chart={elem} handleChartClick={this.handleChartClick} />;
              })}
            </div>
          </div>


            );
  }

}
