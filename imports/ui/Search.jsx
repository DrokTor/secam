import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Assets from 'meteor/meteor';
import alertify from 'alertify.js';

export default class Search extends Component {

  constructor(props){
    super(props);
    //console.log(this.props.patient);

      this.state = {
        searchText: this.props.text || '',
        advancedSearch: false,
        chartType: '',
      };

    //console.log(this.state);
    this.handleChange=this.handleChange.bind(this);
    this.advancedSearch=this.advancedSearch.bind(this);
    this.resetSearch=this.resetSearch.bind(this);
    this.chartChoice=this.chartChoice.bind(this);
    this.createChart=this.createChart.bind(this);
  }

  clickSearch(){
    //$('.simple-search').toggle("collapse");
    //$('.searchs').toggle("col-3");
    //$('.searchs').toggle("col-1");
     //$('.simple-search').fadeToggle('fast');
     //$('.simple-search').toggleClass('fade in');
  }

/*
  componentDidUpdate(){
    (this.props.text)?
    this.setState({
      searchText:  this.props.text ,
    })
    : '' ;
  }
*/
  advancedSearch(e){
    //e.preventDefault();
    //console.log(e.target);
    this.setState({
      advancedSearch:  !this.state.advancedSearch ,
    })

  }

  handleChange(event) {
    event.preventDefault();
    let search ={};
    search[event.target.dataset.field] = event.target.value;
    this.setState(search,  () => {
      //console.log(this.state);
      this.props.search(this.state);
      if(!this.props.tagsection){
      (this.state.searchText!='') ?
      $('#create-chart').removeClass('collapse')
      :
      $('#create-chart').addClass('collapse');
      }
    });

  }
  resetSearch(event) {
    //event.preventDefault();
    this.setState({
      searchText: '',
    },()=>{
      this.props.search(this.state);
      $('#create-chart').addClass('collapse');
    });

  }

  chartChoice(changeEvent) {
    this.setState({
      chartType: changeEvent.target.value
    });
  }

  createChart(event) {
    //event.preventDefault();
    const chartName = ReactDOM.findDOMNode(this.refs['chartName']).value,
    chartFocus = ReactDOM.findDOMNode(this.refs['chartFocus']).value,
    chartType = this.state.chartType;


      const chart = { name : chartName, search : this.state.searchText, focus : chartFocus  , type : chartType  }
      Meteor.call('charts.addChart',chart,()=>{
        alertify.success('Item rajouté avec succès:' + chartName);
        this.props.showDashboard();
      });
  }

  cancelCreateChart(){
    alertify.error('Action annulée');
  }

  render(){

      return(
        <div>
        <h5 className="offset-md" >Clients {this.props.total? '('+this.props.total+')':''}
        <div className="container search row   float-right ">
          <div  className="col-1 offset-1 " /*onClick={this.createChart}*/>
            <div id="create-chart" className={(this.state.searchText && !this.props.tagsection)? "":"collapse"}>
              <i className="pointer fa fa-pie-chart " data-toggle="modal" data-target="#ChartModal"></i>
            </div>
          </div>
          <div className="col-8  simple-search "> {/*collapse*/}
            <div className="row">
            <div className="col-9 ">
              <div className="row">
               <input
                  className="col-10 search-field "
                  type="text"
                  type="textInput"
                  ref="searchText"
                  data-field="searchText"
                  value={this.state.searchText}
                  placeholder="recherche"
                  onChange={ this.handleChange }
                />
                <div className="col-2 pointer" onClick={this.resetSearch}>X</div>
              </div>
            </div>
            { <div className="col-3 ">
              <div className="row">
              <label className="row switch-light switch-material" >
               <input className="" type="checkbox" onClick={this.advancedSearch} />
                 <strong className="col-6 ">
                   Boost
                 </strong>

                 <span className="float-right alert alert-light">
                   <span>Off</span>
                   <span>On</span>
                   <a className=" "></a>
                 </span>
                 </label>
               </div>
             </div> }
             </div>

         </div>
         <i className="search-machines col-1 offset-1  fa-lg fa fa-search pointer float-right" ></i> {/* onClick={this.clickSearch}*/}

        </div>
        </h5>
        {/* chart info modal */}
         <div id="ChartModal" className="modal fade" role="dialog">
           <div className="modal-dialog">
             <div className="modal-content">
               <div className="modal-header">
                 Veuillez choisir les options du graphique.
                 <button type="button" className="close " data-dismiss="modal">&times;</button>
               </div>
               <div className="modal-body">
                 <div className="form-group">
                   <label>Non du graphique</label><br/>
                   <input
                     type="text"
                     ref="chartName"
                   />
                 </div>
                 <div className="form-group">
                   <label>Type du graphique</label><br/>
                   <div className="row">
                     <div className="col offset-1"><input type="radio" ref="chartType" name="chartType" value="bar"  onClick={this.chartChoice} /><i className="offset-1 fa fa-bar-chart" aria-hidden="true"></i></div>
                     <div className="col"><input type="radio" ref="chartType" name="chartType" value="pie"  onClick={this.chartChoice} /><i className="offset-1 fa fa-pie-chart" aria-hidden="true"></i></div>
                     <div className="col"><input type="radio" ref="chartType" name="chartType" value="doughnut"  onClick={this.chartChoice} /><i className="offset-1 material-icons" aria-hidden="true" >donut_small</i></div>
                   </div>
                 </div>
                 <div className="form-group">
                   <label>Présenter en se basant sur</label><br/>
                   <select
                     ref="chartFocus"
                   >
                     <option value="state">État de la machine</option>
                     <option value="os">Système d'exploitation</option>
                     <option value="tag">Tag</option>
                   </select>
                 </div>
               </div>
               <div className="modal-footer">
                 <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.createChart} >Ajouter</button>
                 <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.cancelCreateChart} >Annuler</button>
               </div>
             </div>

           </div>
         </div>
       </div>

            );
  }

}
