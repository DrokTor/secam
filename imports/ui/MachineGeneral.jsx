import React, { Component } from 'react';
import PropTypes from 'prop-types';
//import moment from 'momentjs/moment';

// Client component - represents a single machine item
export default class MachineGeneral extends Component {

  constructor(props){
      super(props);

      this.searchTag = this.searchTag.bind(this);
  }

  searchTag(e){
    this.props.searchTag(e.target.dataset.tagname);
  }
  render() {
    moment.locale('fr');
    //console.log(this.props.machine);
    const machine = this.props.machine;
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col">
            <label >OS : </label>
            <span> {machine.os} </span>
          </div>

        </div>
        <div className="row">
          <div className="col">
            <label >Nom : </label>
            <span> {machine.name} </span>
          </div>
          <div className="col">
            <label >Etat : </label>
            <span className={machine.state.toLowerCase()}>{machine.state}</span>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <label >IP : </label>
            <span> {machine.ip} </span>
          </div>
          <div className="col">
            <label >Scan : </label>
            <span> {moment(machine.lastUpdate).format('DD MMM YYYY - HH:mm ').toString()}</span>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <label >Tags : </label>
            <span> { (machine.tags)? machine.tags.map((tag,index)=>{
              return <span key={index} className="pointer badge badge-info" onClick={this.searchTag} data-tagname={tag} > {tag} </span>
            }) : '' }</span>
          </div>
        </div>
      </div>
    );
  }
}

// Client.propTypes = {
//   // This component gets the task to display through a React prop.
//   // We can use propTypes to indicate it is required
//   //client: PropTypes.object.isRequired,
// };
