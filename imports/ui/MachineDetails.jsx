import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';

//import moment from 'momentjs/moment';
import { Integrity } from '../api/integrity.js';

import MachineGeneral from './MachineGeneral.jsx';
import MachineConfig from './MachineConfig.jsx';
import MachineSoftware from './MachineSoftware.jsx';
import MachineProcess from './MachineProcess.jsx';
import MachineIntegrity from './MachineIntegrity.jsx';

// Client component - represents a single machine item
export  class MachineDetails extends Component {

  constructor(props){
    super(props);
    this.state = {
      machineMenu : "Général",
    }
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(e){
    this.setState({
      machineMenu: e.target.dataset.menu
    })
  }
  render() {
    moment.locale('fr');

    return (
      <div className="row">
        <div className="col-md-3">
          <ul className="machine-menu col-md-12">
            {/* this.props.menu.map((menu)=>{
              return <li key={menu._id} onClick={this.handleClick} data-menu={menu.menu}>{menu.menu}</li>
            })*/}
             <li   onClick={this.handleClick} data-menu={'Général'}>{'Général'}</li>
             <li   onClick={this.handleClick} data-menu={'Logiciels'}>{'Logiciels'}</li>
             <li   onClick={this.handleClick} data-menu={'Processus'}>{'Processus'}</li>
             <li   onClick={this.handleClick} data-menu={'Contrôle d\'intégrité'}>{'Contrôle d\'intégrité'}</li>
          </ul>
        </div>
        <div className="machine-content col-md-9">
          {
             (() => {
               switch(this.state.machineMenu) {
                 case "Général":
                   return <MachineGeneral machine={this.props.machine} searchTag={this.props.searchTag} />;
                     break;
                 case "Config":
                   return <MachineConfig />;
                     break;
                 case "Logiciels":
                   return <MachineSoftware software={this.props.machine.software} />;
                     break;
                 case "Processus":
                   return <MachineProcess process={this.props.machine.process} />;
                     break;
                 case "Contrôle d'intégrité":
                   return <MachineIntegrity  integrity = {this.props.integrity} />;
                     break;
                 default:
                   return "menu non disponible";
                   }
            })()

            }
        </div>
      </div>
    );
  }
}

// Client.propTypes = {
//   // This component gets the task to display through a React prop.
//   // We can use propTypes to indicate it is required
//   //client: PropTypes.object.isRequired,
// };
export default withTracker(() => {

  Meteor.subscribe('integrity');

  const integrityData = Integrity.find({}).fetch();

  console.log(integrityData);
  return{
    integrity : integrityData,
  };

})(MachineDetails)
