import React, { Component } from 'react';
import PropTypes from 'prop-types';
//import moment from 'momentjs/moment';

// Client component - represents a single machine item
export default class Client extends Component {

  handleClick(){
    this.props.more(this.props.client);
  }
  /*alias(name){
    const hostWords = name.split(' ');
    //console.log(hostWords);
    let alias = name;
    if(hostWords[1] == 'Windows' && hostWords[2] == 'Server'){
     alias = hostWords[1]+' '+hostWords[3];
     //console.log(alias);
   }else if (hostWords[1] == 'Windows') {
     alias = hostWords[1]+' '+hostWords[2];
     //console.log(alias);
    }
    return alias;
  };*/
  render() {
    state= this.props.client.state;
    moment.locale('fr');
     {/*this.alias(this.props.client.os)*/}
    return (
      <tr className="row" onClick={this.handleClick.bind(this)}>
        <td className="col-2">{this.props.client.name}</td>
        <td className="col-2">{this.props.client.aliasOs}</td>
        <td className="col-2">{this.props.client.ip}</td>
        <td className="col-3">{moment(this.props.client.lastUpdate).format('DD MMM YYYY | HH:mm ').toString()}</td>
        <td className="col-2" >{ (this.props.client.tags)? this.props.client.tags.map((tag,index)=>{
          return <span key={index} className="badge badge-info"> {tag} </span>
        }) : '' }</td>
        <td className={"col-1 "+state.toLowerCase()} >{state}</td>
      </tr>
    );
  }
}

Client.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  client: PropTypes.object.isRequired,
};
