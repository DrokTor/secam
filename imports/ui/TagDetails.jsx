import React, { Component } from 'react';
import PropTypes from 'prop-types';
//import moment from 'momentjs/moment';
import alertify from 'alertify.js';
// Client component - represents a single machine item
export default class TagDetails extends Component {

  constructor(props){
    //console.log(alertif);
    super(props);

    /*this.state = {
      name:'',
      type:'Statique',
      //tagParentID:'',
    };*/

    this.assignInterface = this.assignInterface.bind(this);
    this.clearTagMachines = this.clearTagMachines.bind(this);
    this.deleteTag = this.deleteTag.bind(this);
    this.searchTagMachines = this.searchTagMachines.bind(this);
  }

  assignInterface(e){
    e.preventDefault();
    this.props.assignInterface();
  }
  clearTagMachines(e){
    e.preventDefault();
    alertify.confirm("êtes-vous sûr de vouloir vider ce tag ?",() => {
      Meteor.call('clients.clearTag',this.props.tag.name,()=>{
        alertify.success('Le tag vient d\'être vidé.');
      });
    }, () =>{
      alertify.log('action annulé.');
    });
  }
  deleteTag(e){
    e.preventDefault();
    alertify.logPosition("bottom right");
    alertify.confirm("êtes-vous sûr de vouloir supprimer ce tag ?",() => {
      Meteor.call('tags.deleteTag',this.props.tag.name,(error,result)=>{
        result?  alertify.success('Le tag vient d\'être supprimé avec succès.') :
        alertify.error('Merci de vider le tag avant de le supprimer.');
      });
    }, () =>{
      alertify.log('action annulé.');
    });
  }
  searchTagMachines(e){
    e.preventDefault();
    this.props.searchTag(this.props.tag.name);
  }

  render() {
    //state= this.props.client.state;
    //moment.locale('fr');
    //console.log(this.props.tagsList);
    const tag = this.props.tagsList.find((tag)=>{ return tag.name == this.props.tag.name }) || {};
    //console.log(tag);
    return (
      <div>
        <div>
          <div className="row"><label className="col-3" >Nom: </label> <span className="col-6" > {/*this.props.*/tag.name}</span></div>
          <div className="row"><label className="col-3" >Type: </label> <span className="col-6" > {/*this.props.*/tag.type}</span></div>
          <div className="row"><label className="col-3" >Lié à: </label> <span className="col-6" > "{/*this.props.*/tag.match}"</span></div>
          <div className="row"><label className="col-3" >Total: </label> <span className="col-6" >  {/*this.props.*/tag.count} </span></div>
        </div>
        <div className="row">
          {(typeof(this.props.tag.name)!='undefined')?
          <div className="col-12 btn-group"  >
            <button className="col-3 btn btn-primary float-right" onClick={this.assignInterface} >Affecter</button>
            <button className="col-3 btn btn-info float-right" onClick={this.searchTagMachines} >Afficher</button>
            <button className="col-3 btn btn-warning float-right" onClick={this.clearTagMachines} >Vider</button>
            <button className="col-2 btn btn-danger float-right" onClick={this.deleteTag} ><i className="fa fa-trash" ></i></button>
          </div>
          :''}
        </div>
      </div>
    );
  }
}

TagDetails.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  //client: PropTypes.object.isRequired,
};
