import React, { Component } from 'react';
import PropTypes from 'prop-types';
//import moment from 'momentjs/moment';

// Client component - represents a single machine item
export default class MachineProcess extends Component {

  constructor(props){
      super(props);

      this.searchTag = this.searchTag.bind(this);
  }

  searchTag(e){
    this.props.searchTag(e.target.dataset.tagname);
  }
  render() {
    moment.locale('fr');
    //console.log(this.props.machine);
    const processArray = this.props.process || [];
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col">
            {
              processArray.map((elem,index)=> {
                return <div key={index}> <div>{elem.pid+' '+elem.name}</div> </div>;
              })
            }

          </div>

        </div>


      </div>
    );
  }
}

// Client.propTypes = {
//   // This component gets the task to display through a React prop.
//   // We can use propTypes to indicate it is required
//   //client: PropTypes.object.isRequired,
// };
