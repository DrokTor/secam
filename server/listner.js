import { Meteor } from 'meteor/meteor';
/*
SimpleRest.setMethodOptions('client', { url: 'clients/:client'});

Meteor.methods({
  'client': function () {
    return 5;
  }
});*/

Meteor.method("newMachine",  (content) => {
  //var future = new Future;
  //console.log(content);
  if(typeof content !='undefined')
  {
    //const params = req.params;
    //let client = req.body;
    //client.id = params.agent;
  //console.log(req.body);
    const res = Meteor.call('clients.addMachine',content);

    //return "555";
    return res._str;
  }

}, {
  url: "agents/",
  getArgsFromRequest: function (request) {
    // Let's say we want this function to accept a form-encoded request with
    // fields named `a` and `b`.
    //console.log(request.headers['content-type']);
    if(request.headers['content-type'] == 'application/json'){
      var content = request.body;
      //console.log(request.headers['content-type'] == 'application/json');
      return [content];//[request];
      // Since form enconding doesn't distinguish numbers and strings, we need
      // to parse it manually
      //return [ parseInt(content.a, 10), parseInt(content.b, 10) ];
      //return [ 5, 6 ];
    }
  },
  httpMethod: 'POST',
})

Meteor.method("listner",  (req) => {
  const params = req.params;
  let client = req.body;
  let id = params.agent;
  //console.log(id);
  Meteor.call('clients.receive',client, id);

  //return client;

}, {
  url: "agents/:agent",
  getArgsFromRequest: function (request) {
    // Let's say we want this function to accept a form-encoded request with
    // fields named `a` and `b`.
    var content = request.body;
    //console.log(content);
    return [request];
    // Since form enconding doesn't distinguish numbers and strings, we need
    // to parse it manually
    return [ parseInt(content.a, 10), parseInt(content.b, 10) ];
    //return [ 5, 6 ];
  },
  httpMethod: 'PUT',
})
