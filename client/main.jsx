
import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import AppInit from '../imports/ui/AppInit.jsx';
import '../imports/startup/accounts-config.js';
//import alertifyy from 'alertify.js';

//alertify = alertifyy;
//console.log(alertif);
Meteor.startup(() => {
  render(<AppInit />, document.getElementById('render-target'));
});
